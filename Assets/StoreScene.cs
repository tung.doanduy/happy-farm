using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StoreScene : MonoBehaviour
{
	public Button temp;
	public GameObject thisPet;


	public void CancelButton()
	{
		gameObject.SetActive(false);
		temp.gameObject.SetActive(false);
	}

	public void SellButton()
	{
		gameObject.SetActive(false);
		temp.gameObject.SetActive(false);
		foreach (GameObject i in egg.currentPets)
		{
			if (thisPet.gameObject.GetComponent<SpriteRenderer>().sprite == i.gameObject.GetComponent<SpriteRenderer>().sprite)
			{
				egg.currentPets.Remove(i);
				break;
			}
		}
		Destroy(thisPet);
	}

	public void LoadScene(string sceneName)
    	{
		SceneManager.LoadScene(sceneName);
    	}
}
