using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class egg : MonoBehaviour
{
	public static int type = 0;

	public Sprite egg1, egg2;

	public static List <GameObject> currentPets = new List <GameObject>();

	public GameObject[] pets;
	public GameObject[] pets1;

	public Button continueBtn;

	int count = 0;

	public void OnMouseDown() 
	{
		count++;
		if (count == 5)
		{
			Vector3 pos = new Vector3 (transform.position.x, transform.position.y, 0);
			Destroy(gameObject);

			int r;
			if (type == 1) r = Random.Range(0,10);
			else r = Random.Range(10,20);

			Instantiate(pets[r], pos, Quaternion.identity);
			currentPets.Add(pets1[r]);
			continueBtn.gameObject.SetActive(true);
		}

		else if (count % 2 == 1)
		{
			transform.rotation = Quaternion.Euler(0, 0, 15);
		}

		else if (count % 2 == 0)
		{
			transform.rotation = Quaternion.Euler(0, 0, -15);
		}	
	}

	void Start()
	{
		if (type == 1)
			this.gameObject.GetComponent<SpriteRenderer>().sprite = egg1; 
		else if (type == 2)
			this.gameObject.GetComponent<SpriteRenderer>().sprite = egg2;
	}
}
