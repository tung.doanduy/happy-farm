using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Move : MonoBehaviour
{
	bool checkMove = true;

	public Button sellBtn, cancelBtn;

	public Vector3 newPos;

	void Start()
	{
		newPos = new Vector3(Random.Range(-8f,8f), Random.Range(-4f,4f), 0);
		transform.position = newPos;
	}

    void Update()
    {
	if (checkMove == true)
		Moving();
    }


	void Moving()
	{
		transform.position = Vector3.MoveTowards(transform.position, newPos, 1 * Time.deltaTime);
		if (transform.position == newPos) 
		{
			newPos = new Vector3(Random.Range(-8f,8f), Random.Range(-4f,4f), 0);
		}
	}

	
	void OnMouseEnter()
	{
		checkMove = false;
	}

	void OnMouseExit()
	{
		checkMove = true;
	}

	void OnMouseDown()
	{
		sellBtn.gameObject.SetActive(true);
		cancelBtn.gameObject.SetActive(true);
	}
}
