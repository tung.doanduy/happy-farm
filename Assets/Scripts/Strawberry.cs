using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Strawberry : MonoBehaviour
{
    GameObject strawberry;
    public SpriteRenderer strawberrySprite;
    public Sprite strawberry_1;
    public Sprite strawberry_2;
    public Sprite strawberry_3;
    private float growTime = 100;
    public float lastGrowTime = 0;
    public Slider slider;
    public GameObject soil;
    GameController gameController;
    // Start is called before the first frame update
    void Start()
    {
        strawberry = gameObject;
        slider.value = 0;
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time < lastGrowTime + growTime) strawberrySprite.sprite = strawberry_1;
        if (Time.time >= lastGrowTime + growTime && Time.time < lastGrowTime + growTime * 2) strawberrySprite.sprite = strawberry_2;
        if (Time.time >= lastGrowTime + growTime * 2) strawberrySprite.sprite = strawberry_3;
        slider.value = (Time.time - lastGrowTime) / (growTime * 2) * 100;
    }

    private void OnMouseUpAsButton()
    {
        if (slider.value >= 100)
        {
            gameController.coin += 100;
            gameController.coinNumber.text = gameController.coin.ToString();
            Destroy(gameObject);
        }
    }
}
