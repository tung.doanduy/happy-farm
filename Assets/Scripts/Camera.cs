using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    GameObject cam;
    // Start is called before the first frame update
    void Start()
    {
        cam = gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Up()
    {
        if (cam.transform.position.y < 5)
        {
            cam.transform.position = new Vector3(cam.transform.position.x, cam.transform.position.y + 5.5f, -10);
        }
    }
    public void Down()
    {
        if (cam.transform.position.y > -6)
        {
            cam.transform.position = new Vector3(cam.transform.position.x, cam.transform.position.y - 5.5f, -10);
        }
    }
    public void Left()
    {
        if (cam.transform.position.x > -3.5f)
        {
            cam.transform.position = new Vector3(cam.transform.position.x-3.5f, cam.transform.position.y, -10);
        }
    }
    public void Right()
    {
        if (cam.transform.position.x < 3.5f)
        {
            cam.transform.position = new Vector3(cam.transform.position.x + 3.5f, cam.transform.position.y, -10);
        }
    }
}
