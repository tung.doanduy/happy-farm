using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tomato : MonoBehaviour
{
    GameObject tomato;
    public SpriteRenderer tomatoSprite;
    public Sprite tomato_1;
    public Sprite tomato_2;
    public Sprite tomato_3;
    private float growTime=10;
    public float lastGrowTime = 0;
    public Slider slider;
    public GameObject soil;
    GameController gameController;
    // Start is called before the first frame update
    void Start()
    {
        tomato = gameObject;
        slider.value = 0;
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time < lastGrowTime + growTime ) tomatoSprite.sprite = tomato_1;
        if (Time.time >= lastGrowTime + growTime && Time.time < lastGrowTime + growTime*2) tomatoSprite.sprite = tomato_2;
        if (Time.time >= lastGrowTime + growTime * 2) tomatoSprite.sprite = tomato_3;
        slider.value = (Time.time - lastGrowTime) / (growTime * 2) * 100;
    }
    private void OnMouseUpAsButton()
    {
        if (slider.value >= 100)
        {
            gameController.coin += 10;
            gameController.coinNumber.text = gameController.coin.ToString();
            Destroy(gameObject);
        }  
    }
}
