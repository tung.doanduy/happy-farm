using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    GameObject gameController;
    public GameObject[] selectPanel;
    public Text coinNumber;
    public int coin;
    //public string[] plantName = new string[12];
    //public string[] plantField = new string[12];
    //public float[] lastGrowTime = new float[12];
    //public int plantNum = 0;
    //public GameObject field;
    // Start is called before the first frame update
    void Start()
    {
        gameController = gameObject;
        if (Application.isPlaying)
        {
            LoadGame();
            coinNumber.text = coin.ToString();
        }
    }

    // Update is called once per frame
    void Update()
    {
        SaveGame();
    }
    public bool PanelChecker()
    {
        for (int i = 0; i < selectPanel.Length; i++)
        {
            if (selectPanel[i].activeSelf) return true;
        }
        return false;
    }

    public void SaveGame()
    {
        SaveSystem.SaveGame(this);
    }

    public void LoadGame()
    {
        GameData data = SaveSystem.LoadGame();
        coin = data.coin;
        //plantNum = data.plantNum;
        //for (int i=0;i<plantNum;i++)
        //{
        //    plantName[i] = data.plantName[i];
        //    plantField[i] = data.plantField[i];
        //    lastGrowTime[i] = data.currentTime-data.lastGrowTime[i];
        //    GameObject plant = Instantiate(GameObject.FindGameObjectWithTag(plantName[i]), field.transform.Find(plantField[i]).transform.position, Quaternion.identity);
        //    plant.transform.parent = field.transform.Find(plantField[i]).transform;
        //    plant.tag = "Clone";
        //}    
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void LoadScene(string sceneName)
    {
        SaveGame();
        SceneManager.LoadScene(sceneName);
    }

    public void BuyEgg(Text text)
    {

        if (text.text == "Normal" && coin >= 50)
        {
            egg.type = 1;
            coin = coin - 50;
	    SaveGame();
            coinNumber.text = coin.ToString();
            SceneManager.LoadScene("Egg");
        }

        else if (text.text == "Special" && coin >= 200)
        {
            egg.type = 2;
            coin = coin - 200;
	    SaveGame();
            coinNumber.text = coin.ToString();
            SceneManager.LoadScene("Egg");
        }

    }



	public void SellEgg()
	{
		coin = coin + 30;
		SaveGame();
	}
}