using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Reflection.Emit;

public class Soil : MonoBehaviour
{
    public GameObject tomato;
    public GameObject selectPanel;
    public Button tomatoButton;
    public GameObject carrot;
    public Button carrotButton;
    public GameObject banana;
    public GameObject strawberry;
    GameObject soil;
    public GameObject field;
    GameController gameController;
    // Start is called before the first frame update
    void Start()
    {
        soil = gameObject;
        field = GameObject.FindGameObjectWithTag("Field");
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        selectPanel.SetActive(false);
    }

    private void OnMouseDown()
    {
        if (!soil.transform.parent.CompareTag("Clone")&&!gameController.PanelChecker())
        {
            selectPanel.SetActive(true);
        }
    }
    public void Tomato()
    {
        Vector3 pos = new Vector3(transform.position.x, transform.position.y, 0);
        GameObject plant = Instantiate(tomato, pos, Quaternion.identity);
        plant.transform.parent = transform;
        plant.GetComponent<Tomato>().lastGrowTime = Time.time;
        selectPanel.SetActive(false);
        plant.tag = "Clone";
        //gameController.plantName[gameController.plantNum] = plant.name.Substring(0, plant.name.Length - 7);
        //gameController.lastGrowTime[gameController.plantNum] = plant.GetComponent<Tomato>().lastGrowTime;
        //gameController.plantField[gameController.plantNum] = soil.name;
        //gameController.plantNum++;
    }
    public void Carrot()
    {
        if (gameController.coin >= 10)
        {
            gameController.coin -= 10;
            gameController.coinNumber.text = gameController.coin.ToString();
            Vector3 pos = new Vector3(transform.position.x, transform.position.y, 0);
            GameObject plant = Instantiate(carrot, pos, Quaternion.identity);
            plant.transform.parent = transform;
            plant.GetComponent<Carrot>().lastGrowTime = Time.time;
            selectPanel.SetActive(false);
            plant.tag = "Clone";
            //gameController.plantName[gameController.plantNum] = plant.name.Substring(0, plant.name.Length - 7);
            //gameController.lastGrowTime[gameController.plantNum] = plant.GetComponent<Carrot>().lastGrowTime;
            //gameController.plantField[gameController.plantNum] = soil.name;
            //gameController.plantNum++;
        }
    }
    public void Banana()
    {
        if (gameController.coin >= 20)
        {
            gameController.coin -= 20;
            gameController.coinNumber.text = gameController.coin.ToString();
            Vector3 pos = new Vector3(transform.position.x, transform.position.y, 0);
            GameObject plant = Instantiate(banana, pos, Quaternion.identity);
            plant.transform.parent = transform;
            plant.GetComponent<Banana>().lastGrowTime = Time.time;
            selectPanel.SetActive(false);
            plant.tag = "Clone";
            //gameController.plantName[gameController.plantNum] = plant.name.Substring(0, plant.name.Length - 7);
            //gameController.lastGrowTime[gameController.plantNum] = plant.GetComponent<Banana>().lastGrowTime;
            //gameController.plantField[gameController.plantNum] = soil.name;
            //gameController.plantNum++;
        }
    }
    public void Strawberry()
    {
        if (gameController.coin >= 30)
        {
            gameController.coin -= 30;
            gameController.coinNumber.text = gameController.coin.ToString();
            Vector3 pos = new Vector3(transform.position.x, transform.position.y, 0);
            GameObject plant = Instantiate(strawberry, pos, Quaternion.identity);
            plant.transform.parent = transform;
            plant.GetComponent<Strawberry>().lastGrowTime = Time.time;
            selectPanel.SetActive(false);
            plant.tag = "Clone";
            //gameController.plantName[gameController.plantNum] = plant.name.Substring(0, plant.name.Length - 7);
            //gameController.lastGrowTime[gameController.plantNum] = plant.GetComponent<Strawberry>().lastGrowTime;
            //gameController.plantField[gameController.plantNum] = soil.name;
            //gameController.plantNum++;
        }
    }
}
