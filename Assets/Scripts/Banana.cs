using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Banana : MonoBehaviour
{
    GameObject banana;
    public SpriteRenderer bananaSprite;
    public Sprite banana_1;
    public Sprite banana_2;
    public Sprite banana_3;
    private float growTime = 50;
    public float lastGrowTime = 0;
    public Slider slider;
    public GameObject soil;
    GameController gameController;
    // Start is called before the first frame update
    void Start()
    {
        banana = gameObject;
        slider.value = 0;
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time < lastGrowTime + growTime) bananaSprite.sprite = banana_1;
        if (Time.time >= lastGrowTime + growTime && Time.time < lastGrowTime + growTime * 2) bananaSprite.sprite = banana_2;
        if (Time.time >= lastGrowTime + growTime * 2) bananaSprite.sprite = banana_3;
        slider.value = (Time.time - lastGrowTime) / (growTime * 2) * 100;
    }
    private void OnMouseUpAsButton()
    {
        if (slider.value >= 100)
        {
            gameController.coin += 50;
            gameController.coinNumber.text = gameController.coin.ToString();
            Destroy(gameObject);
        }
    }

}
