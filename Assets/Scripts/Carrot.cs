using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Carrot : MonoBehaviour
{
    GameObject carrot;
    public SpriteRenderer carrotSprite;
    public Sprite carrot_1;
    public Sprite carrot_2;
    public Sprite carrot_3;
    private float growTime = 20;
    public float lastGrowTime = 0;
    public Slider slider;
    public GameObject soil;
    GameController gameController;
    // Start is called before the first frame update
    void Start()
    {
        carrot = gameObject;
        slider.value = 0;
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time < lastGrowTime + growTime) carrotSprite.sprite = carrot_1;
        if (Time.time >= lastGrowTime + growTime && Time.time < lastGrowTime + growTime * 2) carrotSprite.sprite = carrot_2;
        if (Time.time >= lastGrowTime + growTime * 2) carrotSprite.sprite = carrot_3;
        slider.value = (Time.time - lastGrowTime) / (growTime * 2) * 100;
    }
    private void OnMouseUpAsButton()
    {
        if (slider.value >= 100)
        {
            gameController.coin += 30;
            gameController.coinNumber.text = gameController.coin.ToString();
            Destroy(gameObject);
        }
    }
}

